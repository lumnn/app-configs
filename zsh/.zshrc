export ZSH=/usr/share/oh-my-zsh/

ZSH_CUSTOM=$HOME/.config/zsh/
ZSH_THEME="avit"
DISABLE_AUTO_UPDATE="true"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"

ZSH_THEME_AWS_PREFIX="☁️  "
ZSH_THEME_AWS_SUFFIX=" "

plugins=(git)

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh
source /usr/bin/aws_zsh_completer.sh

# Not much ZSH realted

alias lsecsmage="lsec2 | grep 'ECS Instance - Magento' | awk '{print \$3}'"
alias git-remove-merged="git branch --merged | egrep -v \"(^\*|master|develop$)\" | xargs git branch -d"
alias sdc="docker compose"
alias sdce="docker compose exec"
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"
alias gitroot="cd \`git rev-parse --show-toplevel\`"
alias sshbastion="ssh admin@\`aws ec2 describe-instances --filters 'Name=tag:Name,Values=Bastion' --query 'Reservations[*].Instances[*].{ip:PublicDnsName}' --output text\`"
alias gloch='git log --no-merges --format="%C(dim)%as%Creset %C(bold)%s %Creset%Cblue@%an %Creset%C(dim)(%h)"'

# Ensure new tabs in kgx are open in same directory
source /etc/profile.d/vte.sh

source /usr/share/nvm/init-nvm.sh

