# AVIT ZSH Theme

# settings
typeset +H _current_dir="%{$fg_bold[blue]%}%3~%{$reset_color%} "

PROMPT='
$(_user_host)${_current_dir} $(git_prompt_info) $(git_prompt_status) $(dock_status)
%{%(?.${fg[white]}.${fg[red]})%}>%{$reset_color%} '

PROMPT2='%{${fg[white]}%}>%{$reset_color%} '

function _user_host() {
  local me
  if [[ -n $SSH_CONNECTION ]]; then
    me="%n@%m"
  elif [[ $LOGNAME != $USERNAME ]]; then
    me="%n"
  fi
  if [[ -n $me ]]; then
    echo "%{$fg[cyan]%}$me%{$reset_color%}:"
  fi
}

function is_dock_project() {
  if [[ -e "docker-compose.yml" ]] || [[ -e "compose.yml" ]]; then
    echo 1
  fi
}

function dock_status() {
    if [ "$(is_dock_project)" ]; then
        echo -n "🐋 "
        docker compose ps -a --format '{{.Service}}\t{{.Status}}' 2>/dev/null | while read line
        do
            #CONTAINER_LETTER_POSITION=$(echo $line | awk 'match($0,"_"){print RSTART}')
            CONTAINER_LETTER=$(echo "$line" | cut -c1 | tr '[:lower:]' '[:upper:]')
            if [[ $line == *"Up"* ]]; then
                echo -n "%{$fg_bold[green]%}"$CONTAINER_LETTER"%{$fg[blue]%}"
            else
                echo -n "%{$fg[red]%}"$CONTAINER_LETTER"%{$fg[blue]%}"
            fi
        done
    fi
}

MODE_INDICATOR="%{$fg_bold[yellow]%}❮%{$reset_color%}%{$fg[yellow]%}❮❮%{$reset_color%}"

# Git prompt settings
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg[red]%}×%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}A "
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[yellow]%}M "
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}D "
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}M "
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}§ "
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[white]%}U "

# LS colors, made with https://geoff.greer.fm/lscolors/
export LSCOLORS="exfxcxdxbxegedabagacad"
export LS_COLORS='di=34;40:ln=35;40:so=32;40:pi=33;40:ex=31;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:'
export GREP_COLOR='1;33'
