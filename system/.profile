export PATH="$PATH:$HOME/.config/composer/vendor/bin:$HOME/.npm-global/bin:$HOME/.local/bin"
export SUDO_EDITOR=rvim
export EDITOR=vim

export MOZ_USE_XINPUT2=1
export MOZ_ENABLE_WAYLAND=1

export COMPOSER_MEMORY_LIMIT=-1
export PHP_CS_FIXER_IGNORE_ENV=true

# Prevent minimising steam apps when focus lost (clicks on other screens)
export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0

export LPASS_ASKPASS=askpass

# For some reason ssh-agent wont work automatically
export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/gcr/ssh
