print('==== USER TEMPLATES ===')

class PSR2New(object):
  name = "PSR2New"
  style = 'camelCase'

  getter = """
    public function %(getterPrefix)s%(normalizedName)s(): %(typeHint)s
    {
        return $this->%(name)s;
    }
"""

  setter = """
    /**
     * @return static
     */
    public function %(setterPrefix)s%(normalizedName)s(%(typeHint)s $%(name)s)
    {
        $this->%(name)s = $%(name)s;

        return $this;
    }
"""