import sublime
import sublime_plugin

class GotoSelectedFileCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        selected_text = ' '.join(self.view.substr(self.view.expand_to_scope(region.a, "string")) for region in self.view.sel())
        selected_text = self.clean_words(selected_text)

        if selected_text:
            self.view.window().run_command("show_overlay", {"overlay": "goto", "show_files": True, "text": selected_text})

    def clean_words(self, text):
        text = (text
            .replace('"', '')
            .replace("'", '')
            .replace('::', ' ')
            .replace('_', ' ')
            .replace('\r', ' ')
            .replace('\n', ' ')
        )

        return text
