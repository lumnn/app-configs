{
    "variables":
    {
        "bg": "var(adwaita_neutral_8)",
        "bg_dark": "color(var(bg) l(- 5%))",
        "bg_light": "color(var(bg) l(+ 15%))",
        // "bg_light": "var(adwaita_neutral_7)",

        "gtk_blue": "#15539e",

        "adwaita_blue_1": "#99c1f1",
        "adwaita_green_1": "#8ff0a4",
        "adwaita_yellow_1": "#f9f06b",
        "adwaita_orange_1": "#ffbe6f",
        "adwaita_red_1": "#f66151",
        "adwaita_purple_1": "#dc8add",
        "adwaita_brown_1": "#cdab8f",

        "adwaita_blue_2": "#62a0ea",
        "adwaita_green_2": "#57e389",
        "adwaita_yellow_2": "#f8e45c",
        "adwaita_orange_2": "#ffa348",
        "adwaita_red_2": "#ed333b",
        "adwaita_purple_2": "#c061cb",
        "adwaita_brown_2": "#b5835a",

        "adwaita_blue_3": "#3584e4",
        "adwaita_green_3": "#33d17a",
        "adwaita_yellow_3": "#f6d32d",
        "adwaita_orange_3": "#ff7800",
        "adwaita_red_3": "#e01b24",
        "adwaita_purple_3": "#9141ac",
        "adwaita_brown_3": "#9141ac",

        "adwaita_blue_4": "#1c71d8",
        "adwaita_green_4": "#2ec27e",
        "adwaita_yellow_4": "#f5c211",
        "adwaita_orange_4": "#e66100",
        "adwaita_red_4": "#c01c28",
        "adwaita_purple_4": "#813d9c",
        "adwaita_brown_4": "#865e3c",

        "adwaita_blue_5": "#1a5fb4",
        "adwaita_green_5": "#26a269",
        "adwaita_yellow_5": "#e5a50a",
        "adwaita_orange_5": "#c64600",
        "adwaita_red_5": "#a51d2d",
        "adwaita_purple_5": "#613583",
        "adwaita_brown_5": "#63452c",

        "adwaita_neutral_1": "#f6f5f4",
        "adwaita_neutral_2": "#deddda",
        "adwaita_neutral_3": "#c0bfbc",
        "adwaita_neutral_4": "#9a9996",
        "adwaita_neutral_5": "#77767b",
        "adwaita_neutral_6": "#5e5c64",
        "adwaita_neutral_7": "#3d3846",
        "adwaita_neutral_8": "#241f31",

        // tango colors
        "tango_white": "#f6f5f4",
        "tango_red": "#EF2929",
        "tango_green": "#8AE234",
        "tango_orange": "#FCAF3E",
        "tango_yellow": "#FCE94F",
        "tango_chocolate": "#E9B96E",
        "tango_blue": "#729FCF",
        "tango_purple": "#AD7FA8",
        "tango_cyan": "#34E2E2",
        "tango_gray": "#555753",

        "white":     "color(var(adwaita_neutral_1))",
        "red":       "color(var(adwaita_red_1))",
        "red2":      "color(var(adwaita_red_1) s(- 25%))",
        "green":     "color(var(adwaita_green_2))",
        "blue":      "color(var(adwaita_blue_1))",
        "orange":    "color(var(adwaita_orange_1))",
        "orange":    "color(var(adwaita_orange_2))",
        "orange":    "color(var(adwaita_orange_3))",
        "yellow":    "color(var(adwaita_yellow_1) l(+ 15%))",
        // "yellow2":   "color(var(adwaita_yellow_2))",
        "yellow2":   "color(var(adwaita_green_2))",
        "yellow3":   "color(var(yellow2))",
        "yellow4":   "color(var(adwaita_neutral_7))",
        "yellow5":   "color(var(adwaita_neutral_6))",
        "purple":    "color(var(adwaita_purple_1))",

        "gray":      "color(var(adwaita_neutral_3))",
    },
    "globals": {
        "foreground": "color(var(white))",
        "background": "var(bg)",
        "line_highlight": "var(bg_dark)",
        "accent": "var(adwaita_blue_3)",
        "caret": "var(adwaita_neutral_1)",
        "selection": "var(bg_light)",
        "selection_border": "var(bg_light)",
        "guide": "color(var(white) a(0.05))",
        "stack_guide": "color(var(white) a(0.05))",
        "active_guide": "color(var(white) a(0.25))",
        "gutter_foreground": "color(var(white) a(0.10))",
        "find_highlight_foreground": "var(bg)",
        "find_highlight": "var(yellow)",
        "line_diff_width": "2",
        "popup_css": "body, html { background-color: color(var(--background) l(+ 10%)); color: var(--foreground); font-size: 1rem; padding: 0 .5rem; } a { color: var(--bluish); } body, html { --mdpopups-font-mono: 'Iosevka Regular' }",
        "phantom_css": "body, html { border-radius: 3px; background-color: color(var(--background) l(+ 10%)); color: var(--foreground); } a { color: var(--bluish); } body, html { --mdpopups-font-mono: 'Iosevka Regular' }"
    },
    "rules":
    [
        {
            "name": "Comment",
            "scope": "comment, punctuation.definition.comment",
            "font_style": "italic",
        },
        {
            "name": "Punctuation",
            "scope": "punctuation - (punctuation.definition.comment, punctuation.separator.namespace.php)",
            "foreground": "var(gray)"
        },
        {
            "name": "Storage type",
            "scope": "storage.type",
            "foreground": "var(red2)",
        },
        {
            "name": "Storage modifier (extends, public, etc.)",
            "scope": "storage.modifier",
            "font_style": "italic"
        },
        {
            "scope": "keyword.other.unit",
            "foreground": "var(purple)"
        },
        {
            "name": "Library function, but not calls",
            "scope": "support.function - meta.function-call",
            "foreground": "var(purple)"
        },
        {
            "name": "Language variable",
            "scope": "variable.language",
            "foreground": "var(purple)",
        },
        {
            "name": "Inherited class",
            "scope": "entity.other.inherited-class",
            "foreground": "var(yellow2)",
            "font_style": "italic"
        },
        {
            // keywords such as @var in php doc comments
            "name": "documentation keywords",
            "scope": "comment.block.documentation keyword.other, comment.block.documentation meta.class",
            "foreground": "var(gray)",
            "font_style": "italic",
        },
        {
            "name": "Invalid",
            "scope": "invalid",
            "foreground": "var(white)",
            "background": "var(red)"
        },
        {
            "name": "Invalid deprecated",
            "scope": "invalid.deprecated",
            "foreground": "var(white)",
            "background": "var(adwaita_red_5)"
        },
        {
            "name": "markup code",
            "scope": "markup.raw",
            "background": "color(var(bg) l(+ 7.5%))"
        },

        // functions
        {
            "name": "Function argument",
            "scope": "variable.parameter - (source.c | source.c++ | source.objc | source.objc++)",
            "foreground": "var(white)",
            "font_style": "normal"
        },
        {
            "scope": "meta.use support.class",
            "font_style": "normal",
        },
        // php
        {
            "name": "Use statements",
            "scope": "meta.use meta.path, meta.path.php - (entity.name.namespace), meta.use entity.name.class",
            "foreground": "var(blue)",
            // "font_style": "italic"
        },
        {
            "name": "Slashes in classes of function argument types",
            "scope": "meta.class.php meta.block.php meta.function.parameters.php meta.group.php",
            "foreground": "var(blue)",
            // "font_style": "italic"
        },
        {
            "name": "Accessors (php: '->' ; js/twig: '.')",
            "scope": "punctuation.accessor",
            "foreground": "var(red2),"
        },
        {
            "name": "Invoking variable function",
            "scope": "meta.function-call.invoke.php",
            "foreground": "var(blue)",
        },
        // html + xml
        {
            "name": "Tags names and punctuation",
            "scope": "entity.name.tag, punctuation.definition.tag",
            "foreground": "var(blue)"
        },
        // js
        {
            "name": "Constructor call class",
            "scope": "meta.function-call.constructor variable.type",
            "foreground": "var(blue)",
        },
        // json
        {
            // for better differentiation between key and values
            "name": "JSON key",
            "scope": "meta.mapping.key.json string.quoted.double.json",
            "foreground": "var(white)"
        }
    ]
}
