import sublime, sublime_plugin, time

class InsertDateCommand(sublime_plugin.TextCommand):
    def run(self, edit, format="%Y-%m-%d"):
        sel = self.view.sel();
        for s in sel:
            self.view.insert(edit, s.end(), time.strftime(format))
