set number
set autoindent
set expandtab
filetype indent plugin on
set ignorecase
set smartcase
set scrolloff=3
syntax enable

" Recommended

set hidden
set wildmenu
set hlsearch

set cursorline

set background=dark
set encoding=UTF-8

colorscheme molokai

" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'preservim/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()


nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

